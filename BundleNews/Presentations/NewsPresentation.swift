//
//  NewsPresentation.swift
//  BundleNews
//
//  Created by Rooster on 16.03.2019.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class NewsPresentation: NSObject {
    
    let title: String?
    let detail: String?
    let time: String?
    let rssDataId:String?
    
    init(title: String?, detail: String?, time:String?, rssDataId:String?) {
        self.title = title
        self.detail = detail
        self.time = time
        self.rssDataId = rssDataId
        super.init()
    }
}

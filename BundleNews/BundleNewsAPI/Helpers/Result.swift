//
//  Result.swift
//  BundleNews
//
//  Created by Rooster on 16.03.2019.
//  Copyright © 2019 Osman Avni Koçoğlu. All rights reserved.
//

import Foundation

public enum Result<Value> {
    case success(Value)
    case failure(Error)
}

public enum Error: Swift.Error {
    case serializationError(internal: Swift.Error)
    case networkError(internal: Swift.Error)
}
